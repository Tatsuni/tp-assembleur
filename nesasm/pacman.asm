
;;;;;;; Ent�te de la rom ;;;;;;;;;;;
	.inesprg 1 ; 1 banque de 16KB (2 banque de 8KB) pour le programme
	.ineschr 1 ; 1 banque de 8KB pour les image (2 tables de motifs)
	.inesmap 0 ; mapper 0 = NROM, Banque par d�faut
	.inesmir 1 ; Mirroir vertical de l'image de fond

;;;;;;; Le code commence ici ;;;;;;;;
	.code

	.bank 0   ; Banque 0 (8KB de $C000 � $DFFF)
	.org $C000  ; goto location $C000.


Start:
	SEI         ; Arreter les interruption, Met le bit I du registre P (etat du processeur) a 1
	CLD         ; Arrete le mode decimal. Met le bit D du registre P � 0
	LDX #$40
	STX $4017  ;  Met $40 dans le registre de controle de l'APU pour arreter les interruptions de l'APU
	LDX #$FF
	TXS         ; Initialise la pile. Place X ($FF$ comme octet moins significatif de l'adresse de la pile). La pile est toujours entre $0100 et $01FF.
	INX         ; Lorsqu'on incremente X qui contient $FF, C tombe � 0
	STX $2000  ;  Arrete les interruption NMI (Bit 7 du registre Contr�le PPU)
	STX $2001  ;  N'affiche rien (Voir bits 3 � 7 du registre Masque PPU)
	STX $4010  ;  Arrete les interruption logiciel

	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

clrmem:
	LDA #$00
	STA $0000,  x		; Place tous les octets � 0 (", x" correspond a l'adressage indexe avec le registre x)
	STA $0100,  x
	STA $0300,  x
	STA $0400,  x
	STA $0500,  x
	STA $0600,  x
	STA $0700, x
	LDA #$FE
	STA $0200, x		; Placer tous les sprite en dehors de l'�cran
	INX
	BNE clrmem		; Branche si non zero (lorsque x a fait le tour des valeurs de $00 � $FF)

	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

	LDA #1			;Initialise variable direction du pacman
	STA direction
;;;;  Initialise le PPU  ;;;;

LoadPalettes:
 	LDA $2002    		; Lire le registre d'etat du PPU ou annuler le latch de $2006 (s'il y a lieu)
	LDA #$3F	
	STA $2006    		; Place les 8 bits les plus significatifs de l'adresse $3F00 dans $2006
	LDA #$00
	STA $2006    		; Place les 8 bits les moins significatifs de l'adresse $3F00 dans $2006
	LDX #0
LoadPalettesLoop:
	LDA Palette, x		; Charge les donnees de la palette (", x" correspond a l'adressage indexe avec le registre x)
	STA $2007		; Place les informations du PPU (l'adresse d'�criture de $2007 incr�mente automatiquement de 1 octet apr�s chaque ecriture)
	INX			; Prochain index a aller chercher dans la palette en memoire rom (etiquette palette+x)
	CPX #32            
	BNE LoadPalettesLoop 	; La palette est completement copiees si x est � 32=16x2

; Initialisation des sprites
	LDX #0

LoadSpritesData:
	LDA SpriteData, x	; SpriteData est en ROM (lecture seule). Pour pouvoir modifier
	STA $0200, x		; ces sprites, nous transf�rons les donn�es des sprites en RAM
	INX
	CPX #64		; il y a 16 sprites de 4 octets (donc 64 octets) � transf�rer en RAM
	BNE LoadSpritesData
	
LoadName:
	LDA #$20	; Nous utiliserons la table de nom 0 (Adr $2000)
	STA $2006
	LDA #$00
	STA $2006
	LDX #0	; x sera notre index de largeur
	LDY #0	; y sera notre index de hauteur
LoadNameLoop:
	STA $2007	; Mettre la tuille no 0 dans la table
	INX
	CPX #32
	BNE LoadNameLoop	; Lorsque x=32, on a une ligne pleine
	LDX #0
	INY
	CPY #30
	BNE LoadNameLoop	; lorsque y=30, on a terminer (32x30 tuiles)

	LDX #0
LoadAttribute:
	LDA #%00011011	; Changer cette valeur pour changer la sous-palette de couleur � utiliser (2 bits par 2x2 tuiles)
	STA $2007
	INX
	CPX #64
	BNE LoadAttribute ; Il y a 64 attribut

;;;;  Fin de l'initialisation de donn�es  ;;;;

;;;;  Commencer l'affichage du PPU  ;;;;
		; Initialisation des donn�es pour l'interruption NMI
	LDX #$20	; La table de noms #1 d�bute � l'adresse $2000
	LDY #32		; Les 32 premi�res tuiles de la table de nom sont � l'ext�rieur de l'�cran
	LDA #1
	STA var1		; Initialise la tuile � placer � (X, Y) � 1 (Tuile 1)


	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

	JSR activePPU

Forever:
	JMP Forever

NMI:
	LDA $2002		; On s'assure qu'il n'y a pas de latch dans $2006
	STY $2006		; y contient l'octet le plus significatif de l'adresse de tuile a afficher
	STX $2006		; x contient l'octet le moins significatif de l'adresse de tuile a afficher
	LDA var1		; Si var1 = 1 (resp. 2), on affiche le motif de background 1 (resp. 2) 
 	STA $2007


	LDA #$01	; �crire $01 et $00 dans $4016 place l'�tat du controlleur 1 dans $4016 et du controlleur 2 dans $4017
	STA $4016
	LDA #$00
	STA $4016	


lectureCtrl1A:
	LDA $4016
	AND #1
	BEQ	lectureCtrl1B
	;Travail pour le bouton A du controlleur 1
lectureCtrl1B:
	LDA $4016
	AND #1
	BEQ	lectureCtrl1Select
	;Travail pour le bouton B du controlleur 1
lectureCtrl1Select:
	LDA $4016
	AND #1
	BEQ	lectureCtrl1Start
	;Travail pour le bouton Select du controlleur 1
lectureCtrl1Start:
	LDA $4016
	AND #1
	BEQ	lectureCtrl1Haut
	;Travail pour le bouton Start du controlleur 1
lectureCtrl1Haut:
	LDA $4016
	AND #1
	BEQ	lectureCtrl1Bas
	;Travail pour le bouton Haut du controlleur 1
	LDA #3
	STA direction
lectureCtrl1Bas:
	LDA $4016
	AND #1
	BEQ	lectureCtrl1Gauche
	;Travail pour le bouton Bas du controlleur 1
	LDA #4
	STA direction
lectureCtrl1Gauche:
	LDA $4016
	AND #1
	BEQ	lectureCtrl1Droit
	;Travail pour le bouton Gauche du controlleur 1
	LDA #2
	STA direction
lectureCtrl1Droit:
	LDA $4016
	AND #1
	BEQ	compare_direction
	;Travail pour le bouton Haut du controlleur 1
	LDA #1
	STA direction

compare_direction:
	LDA direction
	CMP #1
	BEQ direction_droit
	CMP #2
	BEQ direction_gauche
	CMP #3
	BEQ direction_haut
	CMP #4
	BEQ direction_bas


direction_droit:
	LDA $0203
	CLC
	ADC	#1
	STA $0203
	LDA $0207
	CLC
	ADC	#1
	STA $0207
	LDA $020B
	CLC
	ADC	#1
	STA $020B
	LDA $020F
	CLC
	ADC	#1
	STA $020F
	JMP updateSprite

direction_gauche:
	LDA $0203
	SEC
	SBC	#1
	STA $0203

	LDA $0207
	SEC
	SBC	#1
	STA $0207

	LDA $020B
	SEC
	SBC	#1
	STA $020B

	LDA $020F
	SEC
	SBC	#1
	STA $020F

	;LDA $0202
	;STA $%00000001

	;LDA $0206
	;STA $%00000001

	;LDA $020A
	;STA $%00000001
	
	;LDA $020E
	;STA $%00000001

	JMP updateSprite

direction_bas:
	LDA $0200
	CLC
	ADC	#1
	STA $0200
	LDA $0204
	CLC
	ADC	#1
	STA $0204
	LDA $0208
	CLC
	ADC	#1
	STA $0208
	LDA $020C
	CLC
	ADC	#1
	STA $020C
	JMP updateSprite

direction_haut:
	LDA $0200
	SEC
	SBC	#1
	STA $0200
	LDA $0204
	SEC
	SBC	#1
	STA $0204
	LDA $0208
	SEC
	SBC	#1
	STA $0208
	LDA $020C
	SEC
	SBC	#1
	STA $020C
	JMP updateSprite

updateSprite:	
	LDA #$00
	STA $2003
	LDA #$02
	STA $4014

	RTI             ; retourne de l'interruption
	
activePPU:
	LDA #%10010000		; Active les interruption NMI, table de motif: sprite = 0 et image de fond = 1
	STA $2000
	LDA #%11111110		; Active l'image de fond et les sprites
	STA $2001
	LDA #$00		;Ne pas faire de defilement d'image
	STA $2005
	STA $2005
	RTS


vblankwait:			; Routine qui attend que l'�cran ait termin� de s'afficher (pour �viter les probl�mes visuels)
	BIT $2002		; Bit place les Code de condition N, V, Z.
	BPL vblankwait	; Si le bit 7 est allum�, on a un vblank (BPL = N flag clear, le bit de negatif = bit 7)
	RTS				; Effectue un branchement vers l'adresse sur le dessus de la pile (Voir "JSR vblankwait")



;;;;; La prochaine section est une partie du ROM de la cartouche il est possible de mettre du code ici. Nous allons l'utiliser pour mettre des constantes ;;;;;;

	.bank 1		; Banque 1 (8KB de $E000 � $FFFF)
	.org $E000	; Donnees en lecture seulement

Palette:
	.db $1C, $1C, $1C, $1C, $0D, $0D, $0D, $0D, $01, $01, $01, $01, $1C, $1C, $1C, $1C	; Palette de l'image de fond
	.db $FE,$28,$3E,$20, $FE,$12,$3E,$20, $FE,$27,$3E,$20, $FE,$16,$3E,$20


SpriteData:
	.db $80, $00, %00000000, $88    ; 200, 201, 202, 203
	.db $80, $01, %00000000, $80	; 204, 205, 206, 207
	.db $88, $02, %00000000, $88	; 208, 209, 20A, 20B
	.db $88, $03, %00000000, $80	; 20C, 20D, 20E, 20F

	.org $FFFA	; vecteur d'interruption commence � $FFFA

	.dw NMI		; Interruption NMI (vblank). Adresse $FFFA
	.dw Start	; Interruption Reset (demarrage). Adresse $FFFC
	.dw 0		; Interruption logiciel (instruction BRK). Adresse $FFFE

;;;;; La prochaine section est une partie du ROM qui correspond aux adresse $0000 � $1FFF de la vram du PPU ;;;;;;

	.bank 2        ; change to bank 2
	.org $0000	; Motif de Sprite


;===============PACMAN========================
SpritePacManHautDroit:
	.db %00000000        
	.db %11100000        
	.db %11111000        
	.db %11111100        
	.db %11111110       
	.db %11111000        
	.db %11100000        
	.db %10000000       

	.db %11100000
	.db %00011000
	.db %00000100
	.db %00000010
	.db %00000001
	.db %00000110
	.db %00011000
	.db %01100000
	
SpritePacManHautGauche:
	.db %00000000
	.db %00000111
	.db %00011111
	.db %00111111
	.db %00111111
	.db %01111111
	.db %01111111
	.db %01111111

	.db %00000111
	.db %00011000
	.db %00100000
	.db %01000000
	.db %01000000
	.db %10000000
	.db %10000000
	.db %10000000
	
SpritePacManBasDroit:
	.db %11100000
	.db %11111000
	.db %11111110
	.db %11111100
	.db %11111000
	.db %11100000
	.db %00000000
	.db %00000000

	.db %00011000
	.db %00000110
	.db %00000001
	.db %00000010
	.db %00000100
	.db %00011000
	.db %11100000
	.db %00000000
	
SpritePacManBasGauche:
	.db %01111111
	.db %01111111
	.db %00111111
	.db %00111111
	.db %00011111
	.db %00000111
	.db %00000000
	.db %00000000

	.db %10000000
	.db %10000000
	.db %01000000
	.db %01000000
	.db %00100000
	.db %00011000
	.db %00000111
	.db %00000000

	.bank 2        ; change to bank 2 (banque de 16kb du ROM inclue dans la PPU au d�marrage)
	.org $1000

Background:

BackgroundMotif10:    ; Tuile 0
    .db %11111111, %11111111
    .db %11111111, %11111111
    .db %11111111, %11111111
    .db %11111111, %11111111
    .db %11111111, %11111111
    .db %11111111, %11111111
    .db %11111111, %11111111
    .db %11111111, %11111111
;=================VARIABLE==================================
	.zp	; Zero page bank (memoire rapide $0000 � $00FF).
	.org $0000  

var1:	.ds 1	; Puisque la RAM du nes n'est pas dans la cartouche, l'initialisation n'est pas prise en compte (ne vous attendez pas � avoir 0 dans cette m�moire par d�faut).

direction:	.ds 1	

