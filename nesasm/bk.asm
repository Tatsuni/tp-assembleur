;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Exemple d'affichage de background
;	Affiche un background compos� de 3 tiles diff�rentes
;	Cr�ation: Louis Marchand, 22 ao�t 2016
;	License: MIT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;; Ent�te de la rom ;;;;;;;;;;;
	.inesprg 1 ; 1 banque de 16KB (2 banque de 8KB) pour le programme
	.ineschr 1 ; 1 banque de 8KB pour les image (2 tables de motifs)
	.inesmap 0 ; mapper 0 = NROM, Banque par d�faut
	.inesmir 1 ; Mirroir vertical de l'image de fond

;;;;;;; Le code commence ici ;;;;;;;;
	.code

	.bank 0   ; Banque 0 (8KB de $C000 � $DFFF
	.org $C000  ; goto location $C000.

Start:
	SEI         ; Arreter les interruption, Met le bit I du registre P (etat du processeur) a 1
	CLD         ; Arrete le mode decimal. Met le bit D du registre P � 0
	LDX #$40
	STX $4017  ;  Met $40 dans le registre de controle de l'APU pour arreter les interruptions de l'APU
	LDX #$FF
	TXS         ; Initialise la pile. Place X ($FF$ comme octet moins significatif de l'adresse de la pile). La pile est toujours entre $0100 et $01FF.
	INX         ; Lorsqu'on incremente X qui contient $FF, C tombe � 0
	STX $2000  ;  Arrete les interruption NMI (Bit 7 du registre Contr�le PPU)
	STX $2001  ;  N'affiche rien (Voir bits 3 � 7 du registre Masque PPU)
	STX $4010  ;  Arrete les interruption logiciel

	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

clrmem:
	LDA #$00
	STA $0000,  x		; Place tous les octets � 0 (", x" correspond a l'adressage indexe avec le registre x)
	STA $0100,  x
	STA $0300,  x
	STA $0400,  x
	STA $0500,  x
	STA $0600,  x
	STA $0700, x
	LDA #$FE
	STA $0200, x		; Placer tous les sprite en dehors de l'�cran (Je place mes sprites dans les adresses $0200 � $02FF)
	INX					; Prochaine it�ration
	BNE clrmem		; Branche si non zero (lorsque x a fait le tour des valeurs de $00 � $FF)

	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

;;;;  Initialise le PPU  ;;;;

LoadPalettes:
 	LDA $2002    		; Lire le registre d'etat du PPU ou annuler le latch de $2006 (s'il y a lieu)
	LDA #$3F	
	STA $2006    		; Place les 8 bits les plus significatifs de l'adresse $3F00 dans $2006
	LDA #$00
	STA $2006    		; Place les 8 bits les moins significatifs de l'adresse $3F00 dans $2006
	LDX #$00
LoadPalettesLoop:
	LDA Palette
	STA $2007   ; Place les informations du PPU (l'adresse d'�criture de $2007 incr�mente automatiquement de 1 octet apr�s chaque ecriture) 
	INX		  
	CPX #32       
	BNE LoadPalettesLoop 	; La palette est completement copiees si x est � 32=16x2

LoadName:
	LDA #$20	; Nous utiliserons la table de nom 0 (Adr $2000)
	STA $2006
	LDA #$00
	STA $2006
	LDX #0	; x sera notre index de largeur
	LDY #0	; y sera notre index de hauteur
LoadNameLoop:
	STA $2007	; Mettre la tuille no 0 dans la table
	INX
	CPX #32
	BNE LoadNameLoop	; Lorsque x=32, on a une ligne pleine
	LDX #0
	INY
	CPY #30
	BNE LoadNameLoop	; lorsque y=30, on a terminer (32x30 tuiles)

	LDX #0
LoadAttribute:
	LDA #%00011011	; Changer cette valeur pour changer la sous-palette de couleur � utiliser (2 bits par 2x2 tuiles)
	STA $2007
	INX
	CPX #64
	BNE LoadAttribute ; Il y a 64 attribut

;;;;  Fin de l'initialisation du PPU  ;;;;

;;;;  Commencer l'affichage du PPU  ;;;;
		; Initialisation des donn�es pour l'interruption NMI
	LDX #$20	; La table de noms #1 d�bute � l'adresse $2000
	LDY #32		; Les 32 premi�res tuiles de la table de nom sont � l'ext�rieur de l'�cran
	LDA #1
	STA var1		; Initialise la tuile � placer � (X, Y) � 1 (Tuile 1)


	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

	JSR activePPU	; On initialise le PPU pour effectuer ce que l'on veut (Afficher le background)

Forever:
  JMP Forever     ;Boucle sans fin, le processus se fera lors d'interruption

		;;;;;;;;;;; Le programme ne ce rendra jamais plus loins que le "JMP Forever",
		;;;;;;;;;;; Nous pla�ons donc les routines par la suite.

NMI:	; A chaque vblank, on ajoute une tuile
	LDA $2002		; On s'assure qu'il n'y a pas de latch dans $2006
	STY $2006		; y contient l'octet le plus significatif de l'adresse de tuile a afficher
	STX $2006		; x contient l'octet le moins significatif de l'adresse de tuile a afficher
	LDA var1		; Si var1 = 1 (resp. 2), on affiche le motif de background 1 (resp. 2) 
 	STA $2007

	JSR activePPU   	; On s'assure que le PPU fait ce que l'on veut. Voir activePPU

		; A noter que meme si le vblank se termine, nous n'utilisons plus le PPU
	INX			; On prepare les information pour la prochaine iteration
	CPY #$23	; La table de noms #1 ce temine � l'adresse $23BF
	BEQ NMI10	; Si on a termin� une ligne, on va � NMI10
	CPX #0		; Lorsqu'on a termin� de modifier toutes une 
	BNE NMI20	; plage d'adresses moins significatifs (ex: $2100 � $21FF), 
	INY			; Nous incr�mentons l'octet le plus significatif (ex: Devient $2200)
	JMP NMI20
NMI10:			; Le code qui suit g�re la fin de la table de nom
	CPX #$C0	; � l'adresse $23C0, nous ne sommes plus dans la table de noms #1
	BNE NMI20	; Branchement s'il nous reste encore des tuiles � changer
	LDX #$20	; La table de noms #1 d�bute � l'adresse $2000
	LDY #32		; Les 32 premi�res tuiles de la table de nom sont � l'ext�rieur de l'�cran
	LDA var1
	CMP #1
	BNE NMI15		
	LDA #2			; Si var1 = 1, on met 2 dans var1
	STA var1
	JMP NMI20
NMI15:
	LDA #1			; Si var1 = 2, on met 1 dans var1
	STA var1

NMI20:
	RTI             ; retourne de l'interruption

activePPU:
	LDA #%10010100		; Active les interruption NMI, table de motif: sprite = 0 et image de fond = 1
	STA $2000
	LDA #%11101110		; Active l'image de fond, mais pas les sprites
	STA $2001
	LDA #$00		;Ne pas faire de defilement d'image
	STA $2005
	STA $2005
	RTS
	
vblankwait:			; Routine qui attend que l'�cran ait termin� de s'afficher (pour �viter les probl�mes visuels)
	BIT $2002		; Bit place les Code de condition N, V, Z.
	BPL vblankwait	; Si le bit 7 est allum�, on a un vblank (BPL = N flag clear, le bit de negatif = bit 7)
	RTS				; Effectue un branchement vers l'adresse sur le dessus de la pile (Voir "JSR vblankwait")


;;;;; La prochaine section est une partie du ROM de la cartouche il est possible de mettre du code ici. Nous allons l'utiliser pour mettre des constantes ;;;;;;

	.bank 1		; Banque 1 (8KB de $E000 � $FFFF)
	.org $E000	; Donnees en lecture seulement (ROM)

Palette:
	.db $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01	; Palette de l'image de fond
	.db $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01, $01	; Palette des sprites

	.org $FFFA	; vecteur d'interruption commence � $FFFA

	.dw NMI		; Interruption NMI (vblank). Adresse $FFFA
	.dw Start	; Interruption Reset (demarrage). Adresse $FFFC
	.dw 0		; Interruption logiciel (instruction BRK). Adresse $FFFE


;;;;; La prochaine section est une partie du ROM qui correspond aux adresse $0000 � $1FFF de la vram du PPU ;;;;;;

	.bank 2        ; change to bank 2 (banque de 16kb du ROM inclue dans la PPU au d�marrage)
	.org $0000

	; Ici, on place les tuile pour le sprite

Background:	; Nous pouvons mettre autant de "Label" pour la m�me adresse.

;;;;; La prochaine section n'est pas dans la cartouche, elle est en RAM du NES ;;;;;;

	.zp	; Zero page bank (memoire rapide $0000 � $00FF).
	.org $0000  
		; D�finir les variables ici
var1:	.ds 1	; Puisque la RAM du nes n'est pas dans la cartouche, l'initialisation n'est pas prise en compte (ne vous attendez pas � avoir 0 dans cette m�moire par d�faut).


	.bss 	; RAM (memoire de $0200 � $07FF). Ne pas utiliser la m�moire $0100 � $01FF car c'est la stack. Nous utiliserons $0200 � $02FF pour les spritesNe
	.org $0300